# README #

Scooters 'N Chairs was started out of a deep need in the market to serve the Mobility Community in the USA. We saw firsthand that the care & compassion necessary to service a community of people who deserve to be treated with high-level care was not in place. As a result of this, we set out to build an online store that stocked the highest-rated mobility scooters, wheelchairs, & specialty aids on the market. We do so by employing a team of internet researchers who scour the web searching review sites, comparison sites, & our competitors using our own in-house proprietary criteria to source only the most-liked and valued mobility products. We review and update on a weekly basis to serve you only the highest-quality on the market. We employ these measures to make sure that our customers are happy and get the help they deserve. We are here to answer any questions you have and want you to find the scooter or chair that makes your life as enjoyable and comfortable as it can be. Your mobility is vital to the quality of your life and we understand that. As a result we have secured the most affordable financing options available for your convenience. Please check out our financing page for more info.

https://www.scootersnchairs.com/
https://www.scootersnchairs.com/blogs/blog/pre-buying-checklist
https://www.scootersnchairs.com/blogs/blog/top-powerchairs-2019
https://www.scootersnchairs.com/blogs/blog/mobility-scooters-2019
https://www.scootersnchairs.com/blogs/blog/mobility-faq